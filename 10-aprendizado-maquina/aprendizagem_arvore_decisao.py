def aprendizagem_arvore_decisao(exemplos, atribs, exemplos_pais):
    if not exemplos:
        return valor_maioria(exemplos_pais)
    elif all( (e.meta == exemplos[0].meta for e in exemplos) ):
        return exemplos[0].meta
    elif not atribs:
        return valor_maioria(exemplos)
    else:
        A = max(atribs, key=lambda a: importancia(a, exemplos))
        arvore = No(A) # Teste A é a raiz da árvore
        for vk in A:
            exs = [e for e in exemplos if e.atribs[A] == vk]
            subatribs = list(atribs)
            subatribs.remove(A)
            
            subarvore = aprendizagem_arvore_decisao(
                exs, subatribs, exemplos)
            arvore.ramificar(subarvore, transicao=vk)
        return arvore