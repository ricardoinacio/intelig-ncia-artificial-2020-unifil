SUJO = "X"
LIMPO = "O"

LIMPAR = "l"
IR_DIREITA = "d"
IR_ESQUERDA = "e"

def haSujeira(local):
    return any(n == SUJO for n in local)

def pretty_print_mundo(ambiente, pos):
    print(f'Ambiente: {ambiente} Robo em: {"A" if pos == 0 else "B"}')

def main():
    ambiente = [SUJO for i in range(2)]
    pos = 0
    
    pretty_print_mundo(ambiente, pos)
    while haSujeira(ambiente):
        mensagem = f'Proxima acao ({LIMPAR}/{IR_DIREITA}/{IR_ESQUERDA}): '
        acao = input(mensagem).lower()
        
        if acao == LIMPAR:
            ambiente[pos] = LIMPO
        elif acao == IR_DIREITA:
            pos = 1
        elif acao == IR_ESQUERDA:
            pos = 0
        else:
            print("Entrada Invalida")
        
        pretty_print_mundo(ambiente, pos)

if __name__ == '__main__':
    main()
    