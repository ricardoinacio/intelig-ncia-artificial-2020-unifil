# Código com definição de agentes abstratos a serem utilizados em nossas aulas.

class AgenteAbstrato():
    '''
    Classe abstrata de agentes artificiais racionais.
    '''
    def perceber(self, ambiente):
        ''' A partir do ambiente, forma uma percepcao interna.
        '''
        pass
    
    def definirAcao(self, percepcao):
        ''' Método que recebe a última percepção do agente e define a ação a
            ser tomada.
        
        :param percepcao: conjunto de descrições das percepções do agente no
            último instante.
        :returns: alguma possivel acao
        '''
        raise NotImplementedError
    
    def atuar(self, ambiente):
        ''' Modifica o ambiente de acordo com a acao tomada.
        '''
        pass

class AgenteReflexivo(AgenteAbstrato):
    def __init__(self, regras):
        ''' Inicializa o agente e suas regras de atuação
        
        :param regras: dicionário de condição-ação, no formato estado -> ação
        '''
        self.regras = regras

    def definirAcao(self, percepcao):
        estado = interpretarEntrada(percepcao)
        acao = self.regras[estado]
        return acao
    
    def interpretarEntrada(self, percepcao):
        pass


class AgenteComModelo(AgenteAbstrato):
    def __init__(self, estado, modelo, regras):
        self.estado = estado
        self.modelo = modelo
        self.regras = regras
        
        # Ultima acao realizada, inicialmente nenhuma
        self.acao = None
    
    def definirAcao(self, percepcao):
        atualizarEstado(percepcao)
        acao = self.regras[(estado, modelo)]
        return acao
    
    def atualizarEstado(self, percepcao):
        ''' Atualiza o estado de acordo com os atributos internos (estado,
            acao, modelo) e a nova percepcao do ambiente
        '''
        pass


class AgenteComObjetivo(AgenteAbstrato):
    def __init__(self, estado, problema):
        self.estado = estado
        self.problema = problema
        # Uma sequencia de acoes, inicialmente vazia
        self.seq = []
        # Um objetivo, inicialmente nulo
        self.objetivo = None
    
    def definirAcao(self, percepcao):
        # Se seq estiver vazia
        if not self.seq:
            self.formularObjetivo(percepcao)
            self.formularProblema()
            self.busca()
            if not self.seq:
                return None
        acao = self.seq.pop(0)
        return acao
    
    def formularObjetivo(self, percepcao):
        ''' Formula um novo objetivo para o agente com base no estado.
        
            Ao final, self.objetivo deve estar preenchido.
        '''
        pass
    
    def formularProblema(self):
        ''' Formula um novo problema a ser resolvido, com base no objetivo
            atual.
            
            Ao final, self.problema deve estar preenchido.
        '''
        pass
    
    def busca():
        ''' Monta uma nova sequencia de acoes para resolver o problema atual.
        
            Ao final, self.seq deve conter uma lista de acoes.
        '''
        pass