#!/usr/bin/env python3

class Pessoa():
    def __init__(self, nome, sobrenome, idade):
        self._nome = nome
        self._sobrenome = sobrenome
        self._idade = idade

    def nome_completo(self):
        return f'{self._nome} {self._sobrenome}' 

class Profissional():
    pass

class Aluno(Pessoa,Profissional):
    def __init__(self, nome, sobrenome, idade, matricula):
        super().__init__(nome, sobrenome, idade)
        self._matricula = matricula

def fibonacci(n):
    '''Calcula o enésimo elemento da sequencia de fibonacci
    utilizando o método recursivo.
    '''
    if n in [1, 2]:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

def fibonacci_ternario(n):
    return 1 if n in [1,2]\
        else fibonacci(n-1) + fibonacci(n-2)

def fibonacci_iterativo(n):
    f0, f1 = 0, 1
    for _ in range(n, 0, -1):
        f0, f1 = f1, f0+f1
    
    return f0

def fibonacci_iterativo_pior(n):
    f0, f1 = 0, 1
    while n > 0:
        f0, f1 = f1, f0+f1
        n-=1
    
    return f0

def soma_de(vals):
    total = 0
    for val in vals:
        total += val
    
    return total

def soma_de_args(*args):
    total = 0
    for val in args:
        total += val
    
    return total

def print_dict(**kwargs):
    for chave in kwargs:
        print(chave, kwargs[chave])

def soma_tres(a,b,c):
    return a+b+c

def seq_fib(inicio=0,limite=10):
    for n in range(inicio+1,limite+1):
        yield fibonacci_iterativo(n)

valor_qualquer = 'None'
def testa_escopo():
    valor_interno = 'outro'
    print(valor_qualquer)
    if True:
        valor_true = 'exite'
    
    print(valor_true)

def operador_binario(a,b,func):
    return func(a,b)

# Erro!!!
#print(valor_interno)

def pesquisa(vals, chave):
    for val in vals:
        if chave(val):
            return val

def somador_parcial(a):
    def somador_terminal(b):
        return a+b
    
    return somador_terminal

def contar_vals(vals):
    return sum([1 for _ in vals])