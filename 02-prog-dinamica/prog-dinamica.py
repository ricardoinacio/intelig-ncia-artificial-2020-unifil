#!/usr/bin/env python3

def fibonacci(n):
    '''Calcula o enésimo elemento da sequencia de fibonacci
    utilizando o método recursivo.
    '''
    return 1 if n in [1, 2]\
        else fibonacci(n-1) + fibonacci(n-2)

class Fibonacci():
    def __init__(self):
        self._memo = {1: 1, 2: 1}
    
    def resolve(self, n):
        if n not in self._memo:
            f0 = self.resolve(n-1)
            f1 = self.resolve(n-2)
            self._memo[n] = f0 + f1
        
        return self._memo[n]


def memoize(f):

    memo = {}
    def _f_memoized(*args):
        if args not in memo:
            result = f(*args)
            memo[args] = result
        
        return memo[args]
    
    return _f_memoized

fibonacci = memoize(fibonacci)

# for n in range(100):
#     print(n, fibonacci(n))